package com.example.finalexam.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.finalexam.CreatedUsers
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class RegisterDialog(context: Context): Dialog(context) {


    private lateinit var editTextRegEmail: EditText
    private lateinit var editTextRegPass: EditText
    private lateinit var buttonReg: Button
    private lateinit var textView4: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_dialog)
        init()
        registerListener()

    }
    private fun init(){
        editTextRegEmail = findViewById((R.id.editTextRegEmail))
        editTextRegPass = findViewById((R.id.editTextRegEmail))
        buttonReg = findViewById((R.id.buttonReg))
    }
    private fun registerListener(){
        buttonReg.setOnClickListener {
            val email = editTextRegEmail.text.toString()
            val password = editTextRegPass.text.toString()

            when {

                email.isEmpty() -> {
                    return@setOnClickListener

                }
                password.isEmpty() -> {
                    return@setOnClickListener

                }
            }


            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        dismiss()
                    }
                }
        }
    }

}